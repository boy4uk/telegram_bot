﻿namespace TelegramBot
{
    public class Note
    {
        public int ID { get; set; }
        public long UserID { get; set; }
        public string Content { get; set; }

        public Note()
        {
        }

        public Note(string content, long userID)
        {
            Content = content;
            UserID = userID;
        }
    }
}