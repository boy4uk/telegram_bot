﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBot
{
    public class User
    {
        public long ID { get; set; }
        public string Name { get; set; }

        protected User()
        {
        }
        public User(long id, string name = "")
        {
            ID = id;
            Name = name;
        }
    }
}
