﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBot
{
    public class Tag
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public int NoteID { get; set; }
        public Tag()
        {
        }

        public Tag(string value) =>
            Value = "#" + value;

        public void ConnectToNote(Note note) =>
            NoteID = note.ID;
    }
}