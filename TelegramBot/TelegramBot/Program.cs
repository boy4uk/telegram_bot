﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace TelegramBot
{
    class Program
    {
        static ITelegramBotClient botClient;
        static Dictionary<long, Condition.ConditionType> Conditions = new Dictionary<long, Condition.ConditionType>(); // userID, conditionType
        static Dictionary<long, Note> Relations = new Dictionary<long, Note>(); //userID-note
        static ApplicationContext DataBase = new ApplicationContext();

        static void Main()
        {
            botClient = new TelegramBotClient("783204650:AAHV5lHKcjUs2wBrPnr-v0VjQAEPcLjgl4I");

            var me = botClient.GetMeAsync().Result;
            Console.WriteLine(
              $"Hi! I am Bot {me.Id} and my name is {me.FirstName}."
            );

            Console.WriteLine("***");


            Console.WriteLine("Tags:");
            foreach(var tag in DataBase.Tags)
            {
                Console.Write("    ");
                Console.WriteLine(tag.Value);
            }


            Console.WriteLine("Notes:");
            foreach (var note in DataBase.Notes)
            {
                Console.Write("    ");
                Console.WriteLine($"{DataBase.GetTag(note.ID).Value} - '{note.Content}' - {note.ID}");
            }

            Console.WriteLine("Users:");
            foreach (var user in DataBase.Users)
            {
                Console.Write("    ");
                Console.WriteLine($"{user.ID}");
            }

            botClient.OnMessage += Actions;
            botClient.StartReceiving();
            Thread.Sleep(int.MaxValue);
        }

        static async void Actions(object sender, MessageEventArgs e)
        {
            User CurrentUser;
            Note Note;
            Tag Tag;

            CurrentUser = new User(e.Message.Chat.Id);
            Conditions.AddUser(CurrentUser);
            DataBase.AddUser(CurrentUser);
            DataBase.SaveChanges();

            switch (Conditions[CurrentUser.ID])
            {
                case Condition.ConditionType.waitingForCommand:
                    {
                        var userMessage = e.Message.Text;
                        switch (userMessage)
                        {
                            case "/start":
                                {
                                    await botClient.SendTextMessageAsync(
                                    chatId: e.Message.Chat,
                                    text: $"Hi! I am Bob the Note Maker. Type '/' to see what I can do."
                                    );
                                    Conditions[CurrentUser.ID] = Condition.ConditionType.waitingForCommand;
                                    break;
                                }
                            case "/newnote":
                                {
                                    await botClient.SendTextMessageAsync(
                                    chatId: e.Message.Chat,
                                    text: $"Ok! Type down your note."
                                    );
                                    Conditions[CurrentUser.ID] = Condition.ConditionType.makingNote;
                                    break;
                                }
                            case "/shownotes":
                                {
                                    await botClient.SendTextMessageAsync(
                                    chatId: e.Message.Chat,
                                    text: $"Here are your notes: \n" + DataBase.GetAllNotes(CurrentUser)
                                    );
                                    Conditions[CurrentUser.ID] = Condition.ConditionType.waitingForCommand;
                                    break;
                                }
                            case "/deletenote":
                                {
                                    await botClient.SendTextMessageAsync(
                                    chatId: e.Message.Chat,
                                    text: $"Ok! Type down a HashTag of the note which you want to delete (With #)."
                                    );
                                    Conditions[CurrentUser.ID] = Condition.ConditionType.deletingNote;
                                    break;
                                }
                            default:
                                {
                                    await botClient.SendTextMessageAsync(
                                    chatId: e.Message.Chat,
                                    text: $"Unknown command. Type '/' to see what I can do."
                                    );

                                    Conditions[CurrentUser.ID] = Condition.ConditionType.waitingForCommand;
                                    break;
                                }
                        }
                        break;
                    }

                case Condition.ConditionType.deletingNote:
                    {
                        var userMessage = e.Message.Text;
                        string noteContent = "";
                        foreach (var tag in DataBase.Tags)
                            if (tag.Value.ToLower() == userMessage.ToLower())
                                foreach (var note in DataBase.Notes)
                                    if (note.ID == tag.NoteID)
                                    {
                                        noteContent = note.Content;
                                        DataBase.Remove(note);
                                        DataBase.Remove(tag);
                                    }

                        await botClient.SendTextMessageAsync(
                        chatId: e.Message.Chat,
                        text: $"Success! Your note:\n'{noteContent}'\n has been deleted!"
                        );

                        Conditions[CurrentUser.ID] = Condition.ConditionType.waitingForCommand;
                        break;
                    }

                case Condition.ConditionType.makingNote:
                    {
                        var userMessage = e.Message.Text;
                        Note = new Note(userMessage, CurrentUser.ID);
                        Relations.Add(CurrentUser.ID, Note);
                        DataBase.Notes.Add(Note);
                        DataBase.SaveChanges();

                        await botClient.SendTextMessageAsync(
                        chatId: e.Message.Chat,
                        text: $"Great! Now add a Tag for your note: '{Note.Content}'"
                        );
                        
                        Conditions[CurrentUser.ID] = Condition.ConditionType.makingTag;
                        break;
                    }
                case Condition.ConditionType.makingTag:
                    {
                        var userMessage = e.Message.Text;
                        Tag = new Tag(userMessage);
                        Tag.ConnectToNote(Relations[CurrentUser.ID]);
                        DataBase.Tags.Add(Tag);
                        DataBase.SaveChanges();
                        await botClient.SendTextMessageAsync(
                        chatId: e.Message.Chat,
                        text: $"Success! Your note {Relations[CurrentUser.ID]} with Tag  {Tag.Value} has been added!"
                        );
                        Relations.Remove(CurrentUser.ID);
                        Conditions[CurrentUser.ID] = Condition.ConditionType.waitingForCommand;
                        break;
                    }
            }
        }
    }


    public static class Extentions
    {
        public static void UpdateCondition(this Dictionary<long, Condition.ConditionType> dictionary,
            User user, Condition condition)
        {
            if (!dictionary.ContainsKey(user.ID))
                dictionary.Add(user.ID, condition.CurrentCondition);
            else
                dictionary[user.ID] = condition.CurrentCondition;
        }

        public static void AddUser(this Dictionary<long, Condition.ConditionType> dictionary,
            User user)
        {
            if (!dictionary.ContainsKey(user.ID))
                dictionary.Add(user.ID, Condition.ConditionType.waitingForCommand);
        }

        public static void AddUser(this ApplicationContext dataBase, User inputUser)
        {
            foreach (var user in dataBase.Users)
                if (user.ID == inputUser.ID)
                    return;
            dataBase.Users.Add(inputUser);
        }

        public static string GetAllNotes(this ApplicationContext dataBase, User inputUser)
        {
            var stringBuilder = new StringBuilder();
            foreach (var note in dataBase.Notes)
                if (note.UserID == inputUser.ID)
                {
                    stringBuilder.Append(' ', 5);
                    stringBuilder.Append(dataBase.GetTag(note.ID).Value);
                    stringBuilder.Append(" - ");
                    stringBuilder.Append("'");
                    stringBuilder.Append(note.Content);
                    stringBuilder.Append("'");
                    stringBuilder.Append("\n");
                }
            return stringBuilder.ToString();
        }

        public static Tag GetTag(this ApplicationContext dataBase, int noteID)
        {
            foreach(var tag in dataBase.Tags)
                if (tag.NoteID == noteID)
                    return tag;
            return new Tag("NoTag");
        }
    }
} 