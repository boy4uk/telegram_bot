﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBot
{
    public class Condition
    {
        public enum ConditionType { waitingForCommand, makingNote, makingTag, approveSaving, deletingNote};
        public ConditionType CurrentCondition { get; set; }
    }
}
